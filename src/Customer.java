public class Customer {
    
    String _id;
    int index;
    Boolean isActive;
    String balance;
    String picture;
    int age;
    String eyeColor;
    String name;
    String gender;
    String company;
    String email;
    String phone;
    Address address;
    String registered;
    String favouriteFruit;

    public Customer (
            String _id,
            int index,
            Boolean isActive,
            String balance,
            String picture,
            int age,
            String eyeColor,
            String name,
            String gender,
            String company,
            String email,
            String phone,
            Address address,
            String registered,
            String favouriteFruit
        ){
            this._id = _id;
            this.index = index;
            this.isActive = isActive;
            this.balance = balance;
            this.picture = picture;
            this.age = age;
            this.eyeColor = eyeColor;
            this.name = name;
            this.gender = gender;
            this.company = company;
            this.email = email;
            this.phone = phone;
            this.address = address;
            this.registered = registered;
            this.favouriteFruit = favouriteFruit;
    }

    public void setCustomer (
        String _id,
        int index,
        Boolean isActive,
        String balance,
        String picture,
        int age,
        String eyeColor,
        String name,
        String gender,
        String company,
        String email,
        String phone,
        Address address,
        String registered,
        String favouriteFruit
    ){
        this._id = _id;
        this.index = index;
        this.isActive = isActive;
        this.balance = balance;
        this.picture = picture;
        this.age = age;
        this.eyeColor = eyeColor;
        this.name = name;
        this.gender = gender;
        this.company = company;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.registered = registered;
        this.favouriteFruit = favouriteFruit;
    }

    public void getInfo() {
        System.out.println("Name :" + this.name);
        System.out.println("Age :" + this.age);
        System.out.println("Gender :" + this.gender);
        System.out.println("Phone :" + this.phone);

        System.out.println("Street :" + this.address.street);
        System.out.println("City :" + this.address.city);
        System.out.println("State :" + this.address.state);
        System.out.println("Postcode :" + this.address.postcode);
    }


}
