import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class App {
    public static void main(String[] args) throws Exception {

        JSONParser parser = new JSONParser();
        
        try (FileReader reader = new FileReader("src/customer.json")) {
            Object obj = parser.parse(reader);
            
            JSONArray jsonArray = (JSONArray)obj;
            
            // get array ? from json
            JSONObject jsonObject = (JSONObject)jsonArray.get(1);
            
            String _id = jsonObject.get("_id").toString();
            int index = Integer.parseInt(jsonObject.get("index").toString());
            Boolean isActive = Boolean.parseBoolean(jsonObject.get("isActive").toString());
            String balance = jsonObject.get("balance").toString();
            String picture = jsonObject.get("picture").toString();
            int age = Integer.parseInt(jsonObject.get("age").toString());
            String eyeColor = jsonObject.get("eyeColor").toString();
            String name = jsonObject.get("name").toString();
            String gender = jsonObject.get("gender").toString();
            String company = jsonObject.get("company").toString();
            String email = jsonObject.get("email").toString();
            String phone = jsonObject.get("phone").toString();
            String registered = jsonObject.get("registered").toString();
            String favoriteFruit = jsonObject.get("favoriteFruit").toString();
            
            // get object address
            JSONObject addressObject = (JSONObject)jsonObject.get("address");
            
            String street = addressObject.get("street").toString();
            String city = addressObject.get("city").toString();
            String state = addressObject.get("state").toString();
            int postcode = Integer.parseInt(addressObject.get("postcode").toString());
            // int latitude = Integer.parseInt(addressObject.get("latitude").toString());
            // int longitude = Integer.parseInt(addressObject.get("longitude").toString());
            
            
            
            Address address = new Address(street, city, state, postcode);
            Customer customer = new Customer(_id, index, isActive, balance, picture, age, eyeColor, name, gender, company, email, phone, address, registered, favoriteFruit);
            
            //set Address 
            address.setAddress(street, city, state, postcode);
            
            // swt customer
            customer.setCustomer(_id, index, isActive, balance, picture, age, eyeColor, name, gender, company, email, phone, address, registered, favoriteFruit);
            customer.getInfo();

            // System.out.println(jsonObject);
            // System.out.println("Name: " + name);
            // System.out.println("Age: " + age);
            // System.out.println("Alamat: " + street);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}