public class Address{
    String street;
    String city;
    String state;
    int postcode;
    int latitude;
    int longitude;

    public Address (String street, String city, String state, int postcode) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
    }

    public void setAddress (String street, String city, String state, int postcode) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
    }

    public void info() {
        System.out.println("jalan :" + this.street);
        System.out.println("kota :" + this.city);
        System.out.println("desa :" + this.state);
        System.out.println("kodepos :" + this.postcode);
    }
}
